class SimpleClass{
  //We will start with our null variables
  String? firstName;

  String? lastName;

  String? favouriteApp;

  String? favouriteCity;


  //Am using getter fields because I don't know any other way to retrieve
  //data from a class(let me know if you know another way) ;D
  String get fullName => "$firstName $lastName";

  String get app => "$favouriteApp";

  String get city => "$favouriteCity";
}

void main(){

  SimpleClass classMember = new SimpleClass();

  //The dot notation will help us in giving value to our class variables
  classMember.firstName = "Fikile";

  classMember.lastName = "Gwilika";

  classMember.favouriteApp = "Tachiyomi"; //I love reading manga

  classMember.favouriteCity = "Cape Town"; //always have lived here so this is my favourite City by default

  //I will now print my full name, favourite app and city
  print(classMember.fullName);
  print(classMember.app);
  print(classMember.city);
}